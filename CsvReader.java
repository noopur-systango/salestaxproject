import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class CsvReader implements Reader {
     
    //Delimiter used in CSV file
    private static final String cvsSplitBy = ",";
    public List<String[]> readCsvFile(File f){
    BufferedReader fileReader = null;
    List<String[]>inputlist=new ArrayList<>();	
    try {
        String line = "";
        //Create the file reader
        fileReader = new BufferedReader(new FileReader(f)); 
        fileReader.readLine();
            
    //Read the file line by line starting from the second line
    while ((line = fileReader.readLine()) != null){					
        String[ ] Task2 = line.split(cvsSplitBy);
        inputlist.add(Task2);  
        }
        } 
        catch (IOException e) {
            System.out.println("Error in CsvReader !!!");
        } finally {
            try {
                if(fileReader!=null)
                {
                fileReader.close();
                }
            } catch (IOException e) {
                System.out.println("Error while closing fileReader !!!");
            }
        }
        return inputlist;
    }
 
}