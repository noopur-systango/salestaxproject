                        Assignment 1
                        
Raghu is the logistics manager in a paper company. His company produces paper and sells them
in different countries. As different countries have different sales tax, the price of paper varies in
each country. Raghu is tasked to calculate the price of all products of his company in different
countries.
Your task is to write a program, which Raghu can use to generate the final price of different
products.

INPUT -
A CSV file with Product details:
Headers:- Product-Name, Product-CostPrice, Country

OUTPUT -
Output file should be a CSV with the following fields:
Product-Name, Product-CostPrice, Product-SalesTax, Product-FinalPrice, Country

Note :- For this task you can assume that, all the countries are having same sales tax % on each
product.