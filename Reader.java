import java.io.*;
import java.util.List;

interface Reader
{
    //public static final String cvsSplitBy = ","; 
    public List<String[]> readCsvFile(File f);
}