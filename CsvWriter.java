import java.io.File;
//import java.io.FileReader;
import java.io.FileWriter;
//import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class CsvWriter implements Writer{
 
    public List<String[]> writeCsvFile(List<String[]> processList , String fileName) 
    {
        FileWriter fileWriter = null;            
        try {
            //String fileName = "output.csv";
            fileWriter = new FileWriter(fileName);
 
            File file = new File("output.csv");
            //if file doesn't exists, then create it
            if(!file.exists())
            {
                file.createNewFile();
            }
            //Write the CSV file header
            fileWriter.append(FILE_HEADER.toString());
             
            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);    
            for (String[] rowData : processList)
		    {
		    fileWriter.append("\n");																		
		    fileWriter.append(String.join(" , ", rowData));										
		    }       
            System.out.println("A new CSV file was created successfully !!!");
             
        } catch (Exception e) {
            System.out.println("Error in CsvWriter !!!");
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
            } 
        }
        return processList;
    }
}