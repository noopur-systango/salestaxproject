import java.util.List;

interface Writer
{
    public static final String NEW_LINE_SEPARATOR = "\n";
    //CSV file header
    public static final String FILE_HEADER = "product-name,product-costprice,product-salestax,product-finalprice,country";
    public List<String[]> writeCsvFile(List<String[]> processList , String fileName);     
}
