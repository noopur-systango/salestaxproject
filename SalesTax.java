import java.io.File;
import java.util.List;

public class SalesTax 
{
    //this is a main method	
    public static void main(String[] args)
    {
        Reader csr = new CsvReader(); // obj 1
        Writer csw = new CsvWriter(); // obj 2
        CsvProcess csp = new CsvProcess(); 
        //Inside main
        
        System.out.println("CSV file is now processing:");
        File csv_file = new File("/home/ubox64/Documents/Java Sales Tax/Task2.csv");

        //if file is not empty, read the file 
        if(csv_file.isFile() && csv_file.length()!=0)
        {
        List<String[]> inputList = csr.readCsvFile(csv_file);
        List<String[]> processList = csp.process(inputList);
        csw.writeCsvFile(processList, "output.csv");
        }
        else
        {
        System.out.println("File is empty or does not exists");
        }
    }
}
